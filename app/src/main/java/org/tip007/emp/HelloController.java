package org.tip007.emp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {
    @Autowired
    EmployeeRepository empRepository;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("title", "従業員一覧");
        List<Employee> emplist=empRepository.findAll();
    System.out.println("** size:"+emplist.size());
        model.addAttribute("emplist", emplist);
        return "index";
    }
}
